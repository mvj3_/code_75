// 安装准备环境 file:///Users/mvj3/Documents/android-sdk-macosx/docs/training/basics/firstapp/creating-project.html
// 版本兼容BUG http://stackoverflow.com/questions/7637144/android-requires-compiler-compliance-level-5-0-or-6-0-found-1-7-instead-plea
// 具体Activity.java代码如下：

package cn.mvj3.hellowold;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView; 

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tv = new TextView(this);  
        tv.setText("Hello World");  
        setContentView(tv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }   
}